﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Oreo
{
    class OreoStaticGameSettings
    {
        /// <summary>
        ///     The amount of credits a user will recieve every x minutes
        /// </summary>
        public const int UserCreditsUpdateAmount = 5000;

        /// <summary>
        ///     The amount of pixels a user will recieve every x minutes
        /// </summary>
        public const int UserPixelsUpdateAmount = 5000;

        /// <summary>
        ///     The time a user will have to wait for Credits/Pixels update in minutes
        /// </summary>
        public const int UserCreditsUpdateTimer = 40;
    }
}

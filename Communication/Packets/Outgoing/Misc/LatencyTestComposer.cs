﻿using System;
using Oreo.HabboHotel.GameClients;

namespace Oreo.Communication.Packets.Outgoing.Misc
{
	class LatencyTestComposer : ServerPacket
    {
        public LatencyTestComposer(GameClient Session, int testResponce)
            : base(ServerPacketHeader.LatencyResponseMessageComposer)
        {
            if (Session == null)
                return;

            Session.TimePingedReceived = DateTime.Now;

			WriteInteger(testResponce);
            OreoServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_AllTimeHotelPresence", 1);
        }
    }
}

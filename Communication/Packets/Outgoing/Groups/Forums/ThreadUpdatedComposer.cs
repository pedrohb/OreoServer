﻿using Oreo.HabboHotel.GameClients;
using Oreo.HabboHotel.Groups.Forums;

namespace Oreo.Communication.Packets.Outgoing.Groups
{
	class ThreadUpdatedComposer : ServerPacket
    {
        public ThreadUpdatedComposer(GameClient Session, GroupForumThread Thread)
            : base(ServerPacketHeader.ThreadUpdatedMessageComposer)
        {
			WriteInteger(Thread.ParentForum.Id);

            Thread.SerializeData(Session, this);
        }
    }
}

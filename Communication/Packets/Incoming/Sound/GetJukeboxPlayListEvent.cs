﻿using Oreo.Communication.Packets.Outgoing.Sound;
using Oreo.HabboHotel.GameClients;


namespace Oreo.Communication.Packets.Incoming.Sound
{
    class GetJukeboxPlayListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session.GetHabbo().CurrentRoom != null)
                Session.SendMessage(new SetJukeboxPlayListComposer(Session.GetHabbo().CurrentRoom));
            OreoServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_MusicPlayer", 1);
        }
    }
}

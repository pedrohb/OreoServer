﻿namespace Oreo.Communication.Packets.Incoming.Quests
{
	class GetCurrentQuestEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            OreoServer.GetGame().GetQuestManager().GetCurrentQuest(Session, Packet);
        }
    }
}

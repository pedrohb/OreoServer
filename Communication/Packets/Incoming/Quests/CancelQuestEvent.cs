﻿namespace Oreo.Communication.Packets.Incoming.Quests
{
	class CancelQuestEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            OreoServer.GetGame().GetQuestManager().CancelQuest(Session, Packet);
        }
    }
}

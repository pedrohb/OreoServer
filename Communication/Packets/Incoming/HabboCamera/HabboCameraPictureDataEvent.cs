﻿using Oreo.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.HabboHotel.Rooms.Camera;
using Oreo.Communication.Packets.Outgoing.HabboCamera;

namespace Oreo.Communication.Packets.Incoming.HabboCamera
{
    public class HabboCameraPictureDataEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var len = Packet.PopInt();
            var bytes = Packet.ReadBytes(len);//Not in use when MOD camera
            
            HabboCameraManager.GetUserPurchasePic(Session, true);
            HabboCameraManager.AddNewPicture(Session);
        }
    }
}

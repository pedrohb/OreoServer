﻿using Oreo.Communication.Packets.Outgoing.HabboCamera;
using Oreo.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oreo.Communication.Packets.Incoming.HabboCamera
{
    public class SetRoomThumbnailEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new SendRoomThumbnailAlertComposer());
        }
    }
}

﻿
using System.Linq;


using Oreo.Communication.Packets.Outgoing.Inventory.Achievements;

namespace Oreo.Communication.Packets.Incoming.Inventory.Achievements
{
    class GetAchievementsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new AchievementsComposer(Session, OreoServer.GetGame().GetAchievementManager()._achievements.Values.ToList()));
        }
    }
}

﻿using Oreo.HabboHotel.Rooms;
using Oreo.HabboHotel.Rooms.Trading;

namespace Oreo.Communication.Packets.Incoming.Inventory.Trading
{
    class TradingModifyEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || !Session.GetHabbo().InRoom)
                return;

            Room Room;

            if (!OreoServer.GetGame().GetRoomManager().TryGetRoom(Session.GetHabbo().CurrentRoomId, out Room))
                return;
            
            if (!Room.CanTradeInRoom)
                return;

            Trade Trade = Room.GetUserTrade(Session.GetHabbo().Id);
            if (Trade == null)
                return;

            Trade.Unaccept(Session.GetHabbo().Id);
        }
    }
}
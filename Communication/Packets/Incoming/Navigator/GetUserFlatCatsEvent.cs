﻿using System.Collections.Generic;
using Oreo.HabboHotel.Navigator;
using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Navigator;

namespace Oreo.Communication.Packets.Incoming.Navigator
{
    public class GetUserFlatCatsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null)
                return;

            ICollection<SearchResultList> Categories = OreoServer.GetGame().GetNavigator().GetFlatCategories();

            Session.SendMessage(new UserFlatCatsComposer(Categories, Session.GetHabbo().Rank));
        }
    }
}
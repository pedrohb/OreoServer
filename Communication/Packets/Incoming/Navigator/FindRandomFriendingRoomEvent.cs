﻿using Oreo.Communication.Packets.Outgoing.Rooms.Session;
using Oreo.HabboHotel.Rooms;

namespace Oreo.Communication.Packets.Incoming.Navigator
{
    class FindRandomFriendingRoomEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            var type = Packet.PopString();
            if (type == "predefined_noob_lobby")
            {
                Session.SendMessage(new RoomForwardComposer(4));
                return;
            }

            Room Instance = OreoServer.GetGame().GetRoomManager().TryGetRandomLoadedRoom();
            if (Instance != null)
                Session.SendMessage(new RoomForwardComposer(Instance.Id));
        }
    }
}

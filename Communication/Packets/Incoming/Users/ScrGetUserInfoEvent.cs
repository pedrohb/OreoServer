﻿using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Users;
using Oreo.Communication.Packets.Outgoing.Handshake;

namespace Oreo.Communication.Packets.Incoming.Users
{
    class ScrGetUserInfoEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new ScrSendUserInfoComposer(Session.GetHabbo()));
            Session.SendMessage(new UserRightsComposer(Session.GetHabbo()));
        }
    }
}

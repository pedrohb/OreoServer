﻿using Oreo.HabboHotel.GameClients;


namespace Oreo.Communication.Packets.Incoming.Handshake
{
    public class GetClientVersionEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string Build = Packet.PopString();

            if (OreoServer.SWFRevision != Build)
                OreoServer.SWFRevision = Build;
        }
    }
}
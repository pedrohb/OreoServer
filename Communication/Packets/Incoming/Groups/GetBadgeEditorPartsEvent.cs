﻿using Oreo.Communication.Packets.Outgoing.Groups;

namespace Oreo.Communication.Packets.Incoming.Groups
{
    class GetBadgeEditorPartsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new BadgeEditorPartsComposer(
                OreoServer.GetGame().GetGroupManager().BadgeBases,
                OreoServer.GetGame().GetGroupManager().BadgeSymbols,
                OreoServer.GetGame().GetGroupManager().BadgeBaseColours,
                OreoServer.GetGame().GetGroupManager().BadgeSymbolColours,
                OreoServer.GetGame().GetGroupManager().BadgeBackColours));
        }
    }
}

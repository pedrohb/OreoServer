﻿using Oreo.Communication.Packets.Outgoing.Catalog;
using Oreo.HabboHotel.GameClients;

namespace Oreo.Communication.Packets.Incoming.Catalog
{
    public class GetRecyclerRewardsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new RecyclerRewardsComposer());
        }
    }
}
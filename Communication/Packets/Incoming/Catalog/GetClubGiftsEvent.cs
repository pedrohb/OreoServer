﻿using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Catalog;

namespace Oreo.Communication.Packets.Incoming.Catalog
{
    class GetClubGiftsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {

            Session.SendMessage(new ClubGiftsComposer());
        }
    }
}

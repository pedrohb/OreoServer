﻿using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Rooms.Engine;

namespace Oreo.Communication.Packets.Incoming.Rooms.Engine
{
    class GetFurnitureAliasesEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new FurnitureAliasesComposer());
        }
    }
}

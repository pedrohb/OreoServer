﻿using Oreo.Communication.Packets.Outgoing.Moderation;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.HabboHotel.Rooms;


namespace Oreo.Communication.Packets.Incoming.Rooms.Action
{
    class AmbassadorWarningMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {

            int UserId = Packet.PopInt();
            int RoomId = Packet.PopInt();
            int Time = Packet.PopInt();
            string HotelName = OreoServer.HotelName;

            Room Room = Session.GetHabbo().CurrentRoom;
            RoomUser Target = Room.GetRoomUserManager().GetRoomUserByHabbo(OreoServer.GetUsernameById(UserId));
            if (Target == null)
                return;

            long nowTime = OreoServer.CurrentTimeMillis();
            long timeBetween = nowTime - Session.GetHabbo()._lastTimeUsedHelpCommand;
            if (timeBetween < 60000)
            {
                Session.SendMessage(RoomNotificationComposer.SendBubble("Abuso", "Espere pelo menos 1 minuto para enviar um alerta de novo.", ""));
                return;
            }

            else
                OreoServer.GetGame().GetClientManager().StaffAlert(RoomNotificationComposer.SendBubble("advice", "" + Session.GetHabbo().Username + " acaba de mandar um alerta embaixador a " + Target.GetClient().GetHabbo().Username + ", clique aqui para ir.", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
            Target.GetClient().SendMessage(new BroadcastMessageAlertComposer("<b><font size='15px' color='#c40101'>Mensagem de embaixadores<br></font></b>embaixadores de " + HotelName + " considerar que o seu comportamento não é o melhor. Por favor, reconsidere a sua atitude, antes de um moderador tomar medidas."));

            Session.GetHabbo()._lastTimeUsedHelpCommand = nowTime;
        }
    }
}

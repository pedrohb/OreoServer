﻿using Oreo.HabboHotel.Moderation;
using Oreo.Communication.Packets.Outgoing.Moderation;

namespace Oreo.Communication.Packets.Incoming.Moderation
{
    class GetModeratorTicketChatlogsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || !Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                return;

            int ticketId = Packet.PopInt();

            ModerationTicket ticket = null;
            if (!OreoServer.GetGame().GetModerationManager().TryGetTicket(ticketId, out ticket) || ticket.Room == null)
                return;

            if (ticket.Room == null)
                return;

            Session.SendMessage(new ModeratorTicketChatlogComposer(ticket, ticket.Room, ticket.Timestamp));
        }
    }
}

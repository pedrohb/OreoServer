﻿using Oreo.Communication.Packets.Outgoing.Notifications;
using Oreo.HabboHotel.GameClients;
using Oreo.Core;

namespace Oreo.Communication.Packets.Incoming.Moderation
{
    class AmbassadorAlert : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session.GetHabbo().Rank < ExtraSettings.AmbassadorMinRank) return;
            int userId = Packet.PopInt();
            GameClient user = OreoServer.GetGame().GetClientManager().GetClientByUserID(userId);
            if (user == null) return;
            user.SendMessage(new SuperNotificationComposer("", "${notification.ambassador.alert.warning.title}", "${notification.ambassador.alert.warning.message}", "", ""));
        }
    }
}
﻿using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Quiz;

namespace Oreo.Communication.Packets.Incoming.Quiz
{
    class PostQuizAnswersMessageEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new PostQuizAnswersMessageComposer(Session));
        }
    }
}

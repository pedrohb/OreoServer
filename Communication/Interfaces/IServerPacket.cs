﻿namespace Oreo.Communication.Interfaces
{
    public interface IServerPacket
    {
        byte[] GetBytes();
    }
}
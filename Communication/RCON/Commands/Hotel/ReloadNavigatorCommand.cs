﻿namespace Oreo.Communication.RCON.Commands.Hotel
{
    class ReloadNavigatorCommand : IRCONCommand
    {
        public string Description => "Se utiliza para atualizar navegador";
        public string Parameters => "";

        public bool TryExecute(string[] parameters)
        {
            OreoServer.GetGame().GetNavigator().Init();

            return true;
        }
    }
}
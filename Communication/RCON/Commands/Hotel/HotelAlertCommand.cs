﻿using System;
using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Moderation;

namespace Oreo.Communication.RCON.Commands.Hotel
{
    class HotelAlertCommand : IRCONCommand
    {
        public string Description => "Este comando se utiliza para mandar alerta a um usuário .";
        public string Parameters => "[MENSAJE]";

        public bool TryExecute(string[] parameters)
        {
            string message = Convert.ToString(parameters[0]);

            OreoServer.GetGame().GetClientManager().SendMessage(new BroadcastMessageAlertComposer(OreoServer.GetGame().GetLanguageManager().TryGetValue("server.console.alert") + "\n\n" + message));
            return true;
        }
    }
}
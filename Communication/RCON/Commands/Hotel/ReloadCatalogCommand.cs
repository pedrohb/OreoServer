﻿using Oreo.Communication.Packets.Outgoing.Catalog;

namespace Oreo.Communication.RCON.Commands.Hotel
{
    class ReloadCatalogCommand : IRCONCommand
    {
        public string Description => "Se utiliza para actualizar Catalogo";
        public string Parameters => "";

        public bool TryExecute(string[] parameters)
        {
            OreoServer.GetGame().GetCatalog().Init(OreoServer.GetGame().GetItemManager());
            OreoServer.GetGame().GetClientManager().SendMessage(new CatalogUpdatedComposer());
            return true;
        }
    }
}
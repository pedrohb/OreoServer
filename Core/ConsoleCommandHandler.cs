﻿using System;
using log4net;
using Oreo.Communication.Packets.Outgoing.Catalog;

using Oreo.Communication.Packets.Outgoing.Moderation;

namespace Oreo.Core
{
    public class ConsoleCommandHandler
    {
        private static readonly ILog log = LogManager.GetLogger("Oreo.Core.ConsoleCommandHandler");

        public static void InvokeCommand(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
                return;

            try
            {
                #region Command parsing
                string[] parameters = inputData.Split(' ');

                switch (parameters[0].ToLower())
                {
                    #region targeted
                    case "atualizartargeted":
                        OreoServer.GetGame().GetTargetedOffersManager().Initialize(OreoServer.GetDatabaseManager().GetQueryReactor());
                        break;
                    #endregion targeted
                    #region stop
                    case "stop":
                    case "desligar":
                        ExceptionLogger.DisablePrimaryWriting(true);
                        log.Warn("O servidor está salvando tudo. Por favor não feche ou desligue o emulador pelo gerenciador de tarefas!");
                        OreoServer.PerformShutDown(false);
                        break;
                    #endregion
                    #region actualizarcatalogo
                    case "atualizarcatalogo":
                        OreoServer.GetGame().GetCatalog().Init(OreoServer.GetGame().GetItemManager());
                        OreoServer.GetGame().GetClientManager().SendMessage(new CatalogUpdatedComposer());
                        break;
                    #endregion
                    #region actualizaritems
                    case "atualizaritems":
                        OreoServer.GetGame().GetItemManager().Init();
                        break;
                    #endregion
                    #region restart
                    case "restart":
                    case "reiniciar":
                        ExceptionLogger.DisablePrimaryWriting(true);
                        log.Warn("O servidor está economizando móveis de usuários, salas, etc. ESPERE QUE O SERVIDOR FECHE, NÃO SAIA DO PROCESSO NO GERADOR DE TAREFAS !!");
                        OreoServer.PerformShutDown(true);
                        break;
                    #endregion
                    #region Clear Console
                    case "limpar":
                    case "clear":
                        Console.Clear();
                        break;
                    #endregion
                    #region alert
                    case "alert":
                    case "alerta":
                        string Notice = inputData.Substring(6);
                        OreoServer.GetGame().GetClientManager().SendMessage(new BroadcastMessageAlertComposer(OreoServer.GetGame().GetLanguageManager().TryGetValue("server.console.alert") + "\n\n" + Notice));
                        log.Info("Alerta enviado com sucesso.");
                        break;
                    #endregion
                    #region navigator
                    case "atualizarnavegador":
                        OreoServer.GetGame().GetNavigator().Init();
                        break;
                    #endregion
                    #region configs
                    case "atualizarconfig":
                        OreoServer.GetGame().GetSettingsManager().Init();
                        ExtraSettings.RunExtraSettings();
                        CatalogSettings.RunCatalogSettings();
                        NotificationSettings.RunNotiSettings();
                        OreoServer.GetGame().GetTargetedOffersManager().Initialize(OreoServer.GetDatabaseManager().GetQueryReactor());
                        break;
                    #endregion

                    default:
                        log.Error(parameters[0].ToLower() + " não existe ou foi digitado errado.");
                        break;
                }
                #endregion
            }
            catch (Exception e)
            {
                log.Error("Erro no comando: [" + inputData + "]: " + e);
            }
        }
    }
}

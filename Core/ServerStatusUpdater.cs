﻿using System;
using System.Threading;
using System.Diagnostics;
using log4net;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel;

namespace Oreo.Core
{
    public class ServerStatusUpdater : IDisposable
    {
        private static ILog log = LogManager.GetLogger("Oreo.Core.ServerStatusUpdater");
        private const int UPDATE_IN_SECS = 15;
        public static int _userPeak;

        private static string _lastDate;

        private static bool isExecuted;

        private static Stopwatch lowPriorityProcessWatch;
        private static Timer _mTimer;

        public static void Init()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            log.Info("Atualização do servidor - Ok! ");
            Console.ResetColor();
            using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.runFastQuery("UPDATE server_status SET status = '1'");
            }
            lowPriorityProcessWatch = new Stopwatch();
            lowPriorityProcessWatch.Start();
        }

        public static void StartProcessing()
        {
            _mTimer = new Timer(Process, null, 0, 10000);
        }

        internal static void Process(object caller)
        {
            if (lowPriorityProcessWatch.ElapsedMilliseconds >= 10000 || !isExecuted)
            {
                isExecuted = true;
                lowPriorityProcessWatch.Restart();

                var clientCount = OreoServer.GetGame().GetClientManager().Count;
                var loadedRoomsCount = OreoServer.GetGame().GetRoomManager().Count;
                var Uptime = DateTime.Now - OreoServer.ServerStarted;
                Game.SessionUserRecord = clientCount > Game.SessionUserRecord ? clientCount : Game.SessionUserRecord;
                Console.Title = string.Concat("Oreo Server [" + OreoServer.HotelName + "] - [" + clientCount + "] Onlines - [" + loadedRoomsCount + "] Quartos - [" + Uptime.Days + "] Dias - [" + Uptime.Hours + "] Horas");

                using (var queryReactor = OreoServer.GetDatabaseManager().GetQueryReactor())
                {
                    if (clientCount > _userPeak) _userPeak = clientCount;

                    _lastDate = DateTime.Now.ToShortDateString();
                    queryReactor.runFastQuery(string.Concat("UPDATE `server_status` SET `status` = '2', `users_online` = '", clientCount, "', `loaded_rooms` = '", loadedRoomsCount, "'"));
                }
            }
        }

        public void Dispose()
        {
            using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.runFastQuery("UPDATE `server_status` SET `users_online` = '0', `loaded_rooms` = '0', `status` = '0'");
            }

            _mTimer.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
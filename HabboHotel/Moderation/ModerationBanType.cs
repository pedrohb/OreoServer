﻿namespace Oreo.HabboHotel.Moderation
{
    public enum ModerationBanType
    {
        IP,
        MACHINE,
        USERNAME
    }
}

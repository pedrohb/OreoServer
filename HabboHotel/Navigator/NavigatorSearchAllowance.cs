﻿namespace Oreo.HabboHotel.Navigator
{
    public enum NavigatorSearchAllowance
    {
        NOTHING,
        SHOW_MORE,
        GO_BACK
    }
}

﻿using System;
using Oreo.Communication.ConnectionManager;
using Oreo.Communication;
using Oreo.Core;
using System.Linq;
using System.Collections.Generic;
using Oreo.Communication.Packets.Incoming;
using Oreo.HabboHotel.Rooms;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel.Users;
using Oreo.HabboHotel.Catalog;
using Oreo.Communication.Interfaces;
using Oreo.HabboHotel.Users.UserData;
using Oreo.Communication.Packets.Outgoing.Sound;
using Oreo.Communication.Packets.Outgoing.Rooms.Chat;
using Oreo.Communication.Packets.Outgoing.Handshake;
using Oreo.Communication.Packets.Outgoing.Navigator;
using Oreo.Communication.Packets.Outgoing.Moderation;
using Oreo.Communication.Packets.Outgoing.Inventory.AvatarEffects;
using Oreo.Communication.Packets.Outgoing.Inventory.Achievements;
using Oreo.Communication.Encryption.Crypto.Prng;
using Oreo.HabboHotel.Users.Messenger.FriendBar;
using Oreo.Communication.Packets.Outgoing.BuildersClub;
using Oreo.HabboHotel.Moderation;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.HabboHotel.Users.Messenger;
using Oreo.HabboHotel.Permissions;
using Oreo.Communication.Packets.Outgoing.Notifications;
using Oreo.Communication.Packets.Outgoing;
using System.Data;

namespace Oreo.HabboHotel.GameClients
{
    public class GameClient
    {
        private readonly int _id;
        private Habbo _habbo;
        public string MachineId;
        private bool _disconnected;
        public ARC4 RC4Client = null;
        private GamePacketParser _packetParser;
        private ConnectionInformation _connection;
        public int PingCount { get; set; }
        internal int CurrentRoomUserId;
        internal DateTime TimePingedReceived;

        public GameClient(int ClientId, ConnectionInformation pConnection)
        {
            _id = ClientId;
            _connection = pConnection;
            _packetParser = new GamePacketParser(this);

            PingCount = 0;
        }

        private void SwitchParserRequest()
        {
            _packetParser.SetConnection(_connection);
            _packetParser.OnNewPacket += Parser_OnNewPacket;
            byte[] data = (_connection.parser as InitialPacketParser).currentData;
            _connection.parser.Dispose();
            _connection.parser = _packetParser;
            _connection.parser.handlePacketData(data);
        }

        private void Parser_OnNewPacket(ClientPacket Message)
        {
            try
            {
                OreoServer.GetGame().GetPacketManager().TryExecutePacket(this, Message);
            }
            catch (Exception e)
            {

				ExceptionLogger.LogException(e);
			}
        }

        private void PolicyRequest()
        {
            _connection.SendData(OreoServer.GetDefaultEncoding().GetBytes("<?xml version=\"1.0\"?>\r\n" +
                   "<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\r\n" +
                   "<cross-domain-policy>\r\n" +
                   "<allow-access-from domain=\"*\" to-ports=\"1-31111\" />\r\n" +
                   "</cross-domain-policy>\x0"));
        }


        public void StartConnection()
        {
            if (_connection == null)
                return;

            PingCount = 0;

            (_connection.parser as InitialPacketParser).PolicyRequest += PolicyRequest;
            (_connection.parser as InitialPacketParser).SwitchParserRequest += SwitchParserRequest;
            _connection.startPacketProcessing();
        }

        public bool TryAuthenticate(string AuthTicket)
        {
            try
            {
				UserData userData = UserDataFactory.GetUserData(AuthTicket, out byte errorCode);
				if (errorCode == 1 || errorCode == 2)
                {
                    Disconnect();
                    return false;
                }

                #region Ban Checking
                //Let's have a quick search for a ban before we successfully authenticate..
                ModerationBan BanRecord = null;
                if (!string.IsNullOrEmpty(MachineId))
                {
                    if (OreoServer.GetGame().GetModerationManager().IsBanned(MachineId, out BanRecord))
                    {
                        if (OreoServer.GetGame().GetModerationManager().MachineBanCheck(MachineId))
                        {
                            Disconnect();
                            return false;
                        }
                    }
                }

                if (userData.user != null)
                {
                    //Now let us check for a username ban record..
                    BanRecord = null;
                    if (OreoServer.GetGame().GetModerationManager().IsBanned(userData.user.Username, out BanRecord))
                    {
                        if (OreoServer.GetGame().GetModerationManager().UsernameBanCheck(userData.user.Username))
                        {
                            Disconnect();
                            return false;
                        }
                    }
                }
                #endregion

                OreoServer.GetGame().GetClientManager().RegisterClient(this, userData.userID, userData.user.Username);
                _habbo = userData.user;
                _habbo.ssoTicket = AuthTicket;
                if (_habbo != null)
                {
                    userData.user.Init(this, userData);

                    SendMessage(new AuthenticationOKComposer());
                    SendMessage(new AvatarEffectsComposer(_habbo.Effects().GetAllEffects));
                    SendMessage(new NavigatorSettingsComposer(_habbo.HomeRoom));
                    SendMessage(new FavouritesComposer(userData.user.FavoriteRooms));
                    SendMessage(new FigureSetIdsComposer(_habbo.GetClothing().GetClothingParts));
                    SendMessage(new UserRightsComposer(_habbo));
                    SendMessage(new AvailabilityStatusComposer());
                    SendMessage(new AchievementScoreComposer(_habbo.GetStats().AchievementPoints));
                    SendMessage(new BuildersClubMembershipComposer());
                    SendMessage(new CfhTopicsInitComposer(OreoServer.GetGame().GetModerationManager().UserActionPresets));
                    SendMessage(new BadgeDefinitionsComposer(OreoServer.GetGame().GetAchievementManager()._achievements));
                    SendMessage(new SoundSettingsComposer(_habbo.ClientVolume, _habbo.ChatPreference, _habbo.AllowMessengerInvites, _habbo.FocusPreference, FriendBarStateUtility.GetInt(_habbo.FriendbarState)));

                    if (GetHabbo().GetMessenger() != null)
                        GetHabbo().GetMessenger().OnStatusChanged(true);

                    if (!string.IsNullOrEmpty(MachineId))
                    {
                        if (_habbo.MachineId != MachineId)
                        {
                            using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                            {
                                dbClient.SetQuery("UPDATE `users` SET `machine_id` = @MachineId WHERE `id` = @id LIMIT 1");
                                dbClient.AddParameter("MachineId", MachineId);
                                dbClient.AddParameter("id", _habbo.Id);
                                dbClient.RunQuery();
                            }
                        }

                        _habbo.MachineId = MachineId;
                    }

					if (OreoServer.GetGame().GetPermissionManager().TryGetGroup(_habbo.Rank, out PermissionGroup PermissionGroup))
					{
						if (!String.IsNullOrEmpty(PermissionGroup.Badge))
							if (!_habbo.GetBadgeComponent().HasBadge(PermissionGroup.Badge))
								_habbo.GetBadgeComponent().GiveBadge(PermissionGroup.Badge, true, this);
					}

					if (!OreoServer.GetGame().GetCacheManager().ContainsUser(_habbo.Id))
                        OreoServer.GetGame().GetCacheManager().GenerateUser(_habbo.Id);

                    _habbo.InitProcess();

                    if (userData.user.GetPermissions().HasRight("mod_tickets"))
                    {
                        SendMessage(new ModeratorInitComposer(
                         OreoServer.GetGame().GetModerationManager().UserMessagePresets,
                         OreoServer.GetGame().GetModerationManager().RoomMessagePresets,
                         OreoServer.GetGame().GetModerationManager().GetTickets));
                    }

                    if (OreoServer.GetGame().GetSettingsManager().TryGetValue("user.login.message.enabled") == "1")
                        SendMessage(new MOTDNotificationComposer(OreoServer.GetGame().GetLanguageManager().TryGetValue("user.login.message")));

                    if (ExtraSettings.WELCOME_MESSAGE_ENABLED)
                    {
                        SendMessage(new MOTDNotificationComposer(ExtraSettings.WelcomeMessage.Replace("%username%", GetHabbo().Username)));
                    }

                    if (GetHabbo().Rank >= 3)
                    {
                        using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("SELECT * FROM `ranks` WHERE id = '" + GetHabbo().Rank + "'");
                            DataRow Table = dbClient.getRow();

                            if (GetHabbo().GetBadgeComponent().HasBadge(Convert.ToString(Table["badgeid"])))
                            {

                            }
                            else
                            {

                                GetHabbo().GetBadgeComponent().GiveBadge(Convert.ToString(Table["badgeid"]), true, GetHabbo().GetClient());
                                SendMessage(RoomNotificationComposer.SendBubble("badge/" + Table["badgeid"], "Você recebeu o emblema staff de acordo com seu rank!", "/inventory/open/badge"));
                            }
                        }
                    }

                    if (ExtraSettings.TARGETED_OFFERS_ENABLED)
                    {
                        if (OreoServer.GetGame().GetTargetedOffersManager().TargetedOffer != null)
                        {
                            OreoServer.GetGame().GetTargetedOffersManager().Initialize(OreoServer.GetDatabaseManager().GetQueryReactor());
                            TargetedOffers TargetedOffer = OreoServer.GetGame().GetTargetedOffersManager().TargetedOffer;

                            if (TargetedOffer.Expire > OreoServer.GetIUnixTimestamp())
                            {

                                if (TargetedOffer.Limit != GetHabbo()._TargetedBuy)
                                {

                                    SendMessage(OreoServer.GetGame().GetTargetedOffersManager().TargetedOffer.Serialize());
                                }
                            }
                            else
                            {
                                using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                                    dbClient.runFastQuery("UPDATE targeted_offers SET active = 'false'");
                                using (var dbClient2 = OreoServer.GetDatabaseManager().GetQueryReactor())
                                    dbClient2.runFastQuery("UPDATE users SET targeted_buy = '0' WHERE targeted_buy > 0");
                            }
                        }
                    }

                    
                    //DateTime dateGregorians = new DateTime();
                    //dateGregorians = DateTime.Today;
                    //int days = (dateGregorians.Day);
                    //if (30 == days)
                    //{
                    //    SendMessage(new HCGiftsAlertComposer());
                    //    SendMessage(new RoomNotificationComposer("sumando", "message", "Hoje é dia de presentes HC, pegue o seu antes que acabe!"));
                    //}

                    
                    string dFrank = null;
                    using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("SELECT Datahoje FROM users WHERE id = '" + userData.user.GetClient().GetHabbo().Id + "' LIMIT 1");
                        dFrank = dbClient.getString();
                    }
                    int dFrankInt = Int32.Parse(dFrank);
                    DateTime dateGregorian = new DateTime();
                    dateGregorian = DateTime.Today;
                    int day = (dateGregorian.Day);
                    if (dFrankInt != day)
                    {
                        using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.RunQuery("UPDATE users SET Datahoje = '" + day + "' WHERE id = " + GetHabbo().Id + ";");
                        }
                        OreoServer.GetGame().GetAchievementManager().ProgressAchievement(userData.user.GetClient(), "ACH_Login", 1);

                    }

                    if (GetHabbo().Rank > Convert.ToInt32(OreoServer.GetConfig().data["MineRankStaff"]))
                    {
                        using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.RunQuery("UPDATE users SET prefix_name = '' WHERE id = '" + GetHabbo().Id + "'");
                            dbClient.RunQuery("UPDATE users SET prefix_name_color = '' WHERE id = '" + GetHabbo().Id + "'");
                        }
                        GetHabbo()._NamePrefixColor = "";
                        GetHabbo()._NamePrefix = "";
                    }

                   

                      

                   
					return true;
				}
			}
			catch (Exception e)
			{
				ExceptionLogger.LogException(e);
			}
			return false;
		}

		public void SendWhisper(string Message, int Colour = 0)
        {
            if (GetHabbo() == null || GetHabbo().CurrentRoom == null)
                return;

            RoomUser User = GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetHabbo().Username);
            if (User == null)
                return;

            SendMessage(new WhisperComposer(User.VirtualId, Message, 0, (Colour == 0 ? User.LastBubble : Colour)));
        }

        public void SendNotification(string Message)
        {
            SendMessage(new BroadcastMessageAlertComposer(Message));
        }

        public void LogsNotif(string Message, string Key)
        {
            SendMessage(new RoomNotificationComposer(Message, Key));
        }

        public void SendMessage(IServerPacket Message)
        {
            byte[] bytes = Message.GetBytes();

            if (Message == null)
                return;

            if (GetConnection() == null)
                return;

            GetConnection().SendData(bytes);
        }

        public void SendShout(string Message, int Colour = 0)
        {
            if (this == null || GetHabbo() == null || GetHabbo().CurrentRoom == null)
                return;

            RoomUser User = GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetHabbo().Username);
            if (User == null)
                return;

            SendMessage(new ShoutComposer(User.VirtualId, Message, 0, (Colour == 0 ? User.LastBubble : Colour)));
        }


        public int ConnectionID
        {
            get { return _id; }
        }

        public ConnectionInformation GetConnection()
        {
            return _connection;
        }

        public Habbo GetHabbo()
        {
            return _habbo;
        }

        public void Disconnect()
        {
            try
            {
                if (GetHabbo() != null)
                {
                    ICollection<MessengerBuddy> Friends = new List<MessengerBuddy>();
                    foreach (MessengerBuddy Buddy in GetHabbo().GetMessenger().GetFriends().ToList())
                    {
                        if (Buddy == null)
                            continue;

                        GameClient Friend = OreoServer.GetGame().GetClientManager().GetClientByUserID(Buddy.Id);
                        if (Friend == null)
                            continue;
                        string figure = this.GetHabbo().Look;

                    }
                    using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.runFastQuery(GetHabbo().GetQueryString);
                    }

                    GetHabbo().OnDisconnect();
                    OreoServer.GetGame().GetClientManager().DispatchEventDisconnect(this);

                }
            }
            catch (Exception e)
            {
                ExceptionLogger.LogException(e);
            }


            if (!_disconnected)
            {
                if (_connection != null)
                    _connection.Dispose();
                _disconnected = true;
            }
        }

        public void Dispose()
        {
            CurrentRoomUserId = -1;

            if (GetHabbo() != null)
                GetHabbo().OnDisconnect();

            MachineId = string.Empty;
            _disconnected = true;
            _habbo = null;
            _connection = null;
            RC4Client = null;
            _packetParser = null;
        }
    }
}
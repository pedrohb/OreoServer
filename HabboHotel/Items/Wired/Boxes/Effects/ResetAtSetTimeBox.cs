﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oreo.HabboHotel.Rooms;
using Oreo.HabboHotel.Users;
using Oreo.Communication.Packets.Incoming;

namespace Oreo.HabboHotel.Items.Wired.Boxes.Effects
{
    internal class ResetAtSetTimeBox : IWiredItem
    {
        public Room Instance { get; set; }
        public Item Item { get; set; }
        public WiredBoxType Type { get { return WiredBoxType.EffectResetAtSetTime; } }
        public ConcurrentDictionary<int, Item> SetItems { get; set; }
        public string StringData { get; set; }
        public bool BoolData { get; set; }
        public int Delay { get { return this._delay; } set { this._delay = value; this.TickCount = value; } }
        public int TickCount { get; set; }
        public string ItemsData { get; set; }
        public int Next { get => _next; set => _next = value; }

        private int _delay = 0;
        private int _next = 0;

        public ResetAtSetTimeBox(Room Instance, Item Item)
        {
            this.Instance = Instance;
            this.Item = Item;
            this.SetItems = new ConcurrentDictionary<int, Item>();

            this.Delay = Delay;
            this.TickCount = Delay;
        }

        public void HandleSave(ClientPacket Packet)
        {
            int Unknown = Packet.PopInt();
            int Unknown2 = Packet.PopInt();
            int Unknown3 = Packet.PopInt();
            int Delay = Packet.PopInt();

            if (Delay > 0)
            {
                int Delay2 = 0;
                while (Delay != 0)
                {
                    Delay -= 65536;
                    Delay2++;
                }
                Delay = Delay2;
            }

            this.Delay = Delay;
            this.TickCount = Delay;
        }

        public bool Execute(params object[] Params)
        {
            if (Instance == null)
                return false;

            Task.Run(async delegate
            {
                int time = Delay * 500;
                await Task.Delay(time);
                Instance.GetWired().TriggerEvent(WiredBoxType.TriggerSetTime, true);
            });

            return true;
        }
    }
}
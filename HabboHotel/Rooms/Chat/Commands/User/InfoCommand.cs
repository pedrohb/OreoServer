﻿using System;
using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.User
{
    class InfoCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_info"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Mostra as informações do servidor"; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            #region
            TimeSpan Uptime = DateTime.Now - OreoServer.ServerStarted;
            int OnlineUsers = OreoServer.GetGame().GetClientManager().Count;
            int RoomCount = OreoServer.GetGame().GetRoomManager().Count;
            int userPeak = OreoServer.GetGame().GetClientManager().GetUserPeak();
            string ClientBuild = OreoServer.SWFRevision;
            #endregion

            Session.SendMessage(new RoomNotificationComposer("Oreo Server 3.3.2",
                "<b>Oreo Server</b> foi baseado no <b>Quasar Server</b> e contém características avançadas:\n\n" +
                 "<b>Créditos:</b>:\n" +
                 "xJoao (Fix)\n" +
                 "Hahn (Fix)\n" +
                 "Butterfly e Plus developers.\n\n" +
                 "<b>Estatísticas do Hotel</b>:\n" +
                 "Usuário Onlines: " + OnlineUsers + "\n" +
                 "Quartos Carregados: " + RoomCount + "\n" +
                 "Pico: " + userPeak + "\n\n" +
                 "<b>Estatísticas do Servidor</b>:\n" +
                 "Uptime: " + Uptime.Days + " dia(s), " + Uptime.Hours + " hora(s) e " + Uptime.Minutes + " minutos.\n", "oreo", ""));
        }
    }
}
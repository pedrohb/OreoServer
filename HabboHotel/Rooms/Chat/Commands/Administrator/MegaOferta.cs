﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oreo.Utilities;
using Oreo.HabboHotel.Users;
using Oreo.HabboHotel.GameClients;
using Oreo.Database.Interfaces;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.Communication.Packets.Outgoing.Catalog;
using Oreo.Core;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class MegaOferta : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_update"; }
        }

        public string Parameters
        {
            get { return "%LIGAR% ou %DESLIGAR%"; }
        }

        public string Description
        {
            get { return "Ligar ou desligar uma mega oferta."; ; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            

            if (Params.Length == 1)
            {
                Session.SendMessage(new RoomNotificationComposer("erro", "message", "Ops, você deve digita assim: ':megaoferta ligar ou :megaoferta desligar'!"));
                return;
            }

            if (Params[1] == "ligar")
            {
                // Comando editaveu abaixo mais cuidado pra não faze merda
                using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE targeted_offers SET active = 'true' WHERE active = 'false'");
                    dbClient.RunQuery("UPDATE users SET targeted_buy = '0'");
                }
                OreoServer.GetGame().GetTargetedOffersManager().Initialize(OreoServer.GetDatabaseManager().GetQueryReactor());
                OreoServer.GetGame().GetClientManager().SendMessage(new RoomNotificationComposer("volada", "message", "Corre, nova mega oferta foi colocada!"));
                Session.SendWhisper("Nova mega oferta iniciada!");
            }

            if (Params[1] == "desligar")
            {
                // Comando editaveu abaixo mais cuidado pra não faze merda
                using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE targeted_offers SET active = 'false' WHERE active = 'true'");
                    dbClient.RunQuery("UPDATE users SET targeted_buy = '0'");
                }
                OreoServer.GetGame().GetTargetedOffersManager().Initialize(OreoServer.GetDatabaseManager().GetQueryReactor());
                OreoServer.GetGame().GetClientManager().SendMessage(new RoomNotificationComposer("ADM", "message", "Que pena, a mega oferta foi retirada!"));
                Session.SendWhisper("Mega oferta retirada!");
            }

            if (Params[1] != "ligar" || Params[1] != "desligar")
            {
                Session.SendMessage(new RoomNotificationComposer("erro", "message", "Ops, você deve digita assim: ':megaoferta ligar ou :megaoferta desligar'!"));
            }
        }
    }
}

﻿
using System.Collections.Generic;

using Oreo.HabboHotel.Rooms;
using Oreo.Core;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
	class RoomUnmuteCommand : IChatCommand
	{
		public string PermissionRequired
		{
			get { return "command_room_unmute"; }
		}

		public string Parameters
		{
			get { return ""; }
		}

		public string Description
		{
			get { return "Desmutar a sala"; }
		}

		public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
		{
           
            if (!Room.RoomMuted)
			{
				Session.SendWhisper("Este quarto não está mutado.");
				return;
			}

			Room.RoomMuted = false;

			List<RoomUser> RoomUsers = Room.GetRoomUserManager().GetRoomUsers();
			if (RoomUsers.Count > 0)
			{
				foreach (RoomUser User in RoomUsers)
				{
					if (User == null || User.GetClient() == null || User.GetClient().GetHabbo() == null || User.GetClient().GetHabbo().Username == Session.GetHabbo().Username)
						continue;

					User.GetClient().SendWhisper("Esta sala foi desmutada .");
				}
			}
		}
	}
}
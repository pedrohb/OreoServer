﻿using Oreo.Core;
using Oreo.HabboHotel.GameClients;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class GOTOCommand : IChatCommand
    {
        public string PermissionRequired => "command_goto";
        public string Parameters => "[IDSALA]";
        public string Description => "Ir a uma sala.";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
          
            if (Params.Length == 1)
            {
                Session.SendWhisper("Você deve especificar uma ID do quarto!");
                return;
            }

            int roomId = 0;
            if (!int.TryParse(Params[1], out roomId))
            {
                Session.SendWhisper("Você deve inserir uma ID de quarto válida");
            }
            else
            {
                Room room = null;
                if (!OreoServer.GetGame().GetRoomManager().TryGetRoom(roomId, out room))
                {
                    Session.SendWhisper("Este quarto não existe!");
                    return;
                }

                Session.GetHabbo().PrepareRoom(room.Id, "");
            }
        }
    }
}
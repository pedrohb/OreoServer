﻿using Oreo.Communication.Packets.Outgoing.Notifications;
using Oreo.Core;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class StaffAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_staff_alert";
        public string Parameters => "[MENSAGE]";
        public string Description => "Enviar mensage a todos os staff.";

        public void Execute(GameClients.GameClient Session, Room Room, string[] Params)
        {
            
            if (Params.Length == 1)
            {
                Session.SendWhisper("Digite uma mensagem para enviar.");
                return;
            }

            string Message = CommandManager.MergeParams(Params, 1);
            OreoServer.GetGame().GetClientManager().StaffAlert(new MOTDNotificationComposer("Mensage da Equipe Staff:\r\r" + Message + "\r\n" + "- " + Session.GetHabbo().Username));
            return;
        }
    }
}
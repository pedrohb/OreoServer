﻿using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.Core;
using Oreo.HabboHotel.Rooms.Chat.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class EndPollCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_give_badge"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Termine a pesquisa na sala atual."; }
        }

        public void Execute(GameClients.GameClient Session, Oreo.HabboHotel.Rooms.Room Room, string[] Params)
        {
            
            Room.EndQuestion();
            Session.SendMessage(new RoomNotificationComposer("game", 3, "Espere um pouquinho que a pesquisa ja vai ser terminada!", ""));
            return;
        }
    }
}

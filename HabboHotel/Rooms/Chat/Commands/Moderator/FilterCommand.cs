﻿using Oreo.Core;
using Oreo.Database.Interfaces;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class FilterCommand : IChatCommand
    {

        public string PermissionRequired => "command_filter";
        public string Parameters => "[PALAVRA]";
        public string Description => "Adicione palavras ao filtro.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
  

            if (Params.Length == 1)
            {
                Session.SendWhisper("Digite uma palavra.");
                return;
            }

            using (IQueryAdapter dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.runFastQuery("INSERT INTO `wordfilter` (id, word, replacement, strict, addedby, bannable) VALUES (NULL, '" + Params[1] + "', '" + OreoServer.HotelName + "', '1', '" + Session.GetHabbo().Username + "', '0')");
            }

            OreoServer.GetGame().GetChatManager().GetFilter().InitWords();
            OreoServer.GetGame().GetChatManager().GetFilter().InitCharacters();
            Session.SendWhisper("Sucesso, continue lutando contra spammers");
        }
    }
}
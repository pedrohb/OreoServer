﻿using Oreo.HabboHotel.GameClients;
using System.Collections.Generic;
using System.Linq;
using Oreo.Communication.Packets.Outgoing.Rooms.Session;
using Oreo.Core;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class MakePrivateCommand : IChatCommand
    {
        public string PermissionRequired => "command_make_public";
        public string Parameters => "";
        public string Description => "Converta esta sala em uma privada.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            
            var room = Session.GetHabbo().CurrentRoom;
            using (var queryReactor = OreoServer.GetDatabaseManager().GetQueryReactor())
                queryReactor.runFastQuery(string.Format("UPDATE rooms SET roomtype = 'private' WHERE id = {0}",
                    room.RoomId));

            var roomId = Session.GetHabbo().CurrentRoom.RoomId;
            var users = new List<RoomUser>(Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUsers().ToList());

            OreoServer.GetGame().GetRoomManager().UnloadRoom(Session.GetHabbo().CurrentRoom);

            RoomData Data = OreoServer.GetGame().GetRoomManager().GenerateRoomData(roomId);
            Session.GetHabbo().PrepareRoom(Session.GetHabbo().CurrentRoom.RoomId, "");

            OreoServer.GetGame().GetRoomManager().LoadRoom(roomId);

            var data = new RoomForwardComposer(roomId);

            foreach (var user in users.Where(user => user != null && user.GetClient() != null))
                user.GetClient().SendMessage(data);
        }
    }
}

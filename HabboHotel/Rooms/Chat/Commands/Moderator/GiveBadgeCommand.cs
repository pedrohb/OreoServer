﻿using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.Core;
using Oreo.HabboHotel.GameClients;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class GiveBadgeCommand : IChatCommand
    {
        public string PermissionRequired => "command_give_badge";
        public string Parameters => "[USUÁRIO] [IDEMBLEMA]";
        public string Description => "Dê um emblema a um usuário.";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
      
            if (Params.Length != 3)
            {
                Session.SendWhisper("Digite um nome de usuário e um código de emblema que você gostaria de dar!");
                return;
            }

            GameClient TargetClient = OreoServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
            if (TargetClient != null)
            {
                if (!TargetClient.GetHabbo().GetBadgeComponent().HasBadge(Params[2]))
                {
                    TargetClient.GetHabbo().GetBadgeComponent().GiveBadge(Params[2], true, TargetClient);
                    if (TargetClient.GetHabbo().Id != Session.GetHabbo().Id)
                        TargetClient.SendMessage(RoomNotificationComposer.SendBubble("badge/" + Params[2], "Você acabou de receber um emblema!", "/inventory/open/badge"));
                    else
                        Session.SendMessage(RoomNotificationComposer.SendBubble("badge/" + Params[2], "Você acabou de dar o emblema: " + Params[2], " /inventory/open/badge"));
                }
                else
                    Session.SendWhisper("Uau, esse usuário já possui este emblema(" + Params[2] + ") !");
                return;
            }
            else
            {
                Session.SendWhisper("Nossa, não conseguimos encontrar o usuário!");
                return;
            }
        }
    }
}

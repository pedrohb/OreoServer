﻿namespace Oreo.HabboHotel.Rooms.Chat
{
    public enum ChatType
    {
        Chat,
        Shout,
        Whisper
    }
}